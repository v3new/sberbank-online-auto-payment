# Установка компонентов для WEBDRIVER.IO
```sh
apt-get install ia32-libs libgconf2 libgconf2-4 xvfb openjdk-7-jre-headless google-chrome
```
# Скачиваем компонент SELENIUM
```sh
cd /var/www/worker_money_send/ && wget -O selenium.jar http://selenium-release.storage.googleapis.com/2.47/selenium-server-standalone-2.47.1.jar
```
# Запускать эмулятор браузера обязательно из под юзера, не обладающего root привелегиями
```sh
adduser %USERNAME% && su %USERNAME% cd /var/www/worker_money_send/ && xvfb-run java -jar selenium.jar -Dwebdriver.chrome.driver=/usr/bin/chromedriver
```
# Настройка Worker'a (файл config.json)
```sh
"imap": {
    "login": "%EMAIL%",                      // почтовый ящик, на который пересылаются сообщения
    "password": "%PASSWORD%",                // пароль от почтового ящика
    "host": "imap.%SERVER%"                  // сервер IMAP
},
    
"app": {
    "port": 3000,                            // порт приложения
    "url": "https://%URL%"                   // url приложения
} 

"auth": {
    "sb": {
            "login": "%LOGIN%",              // логин от сбербанка онлайн
            "password": "%PASSWORD%",        // пароль от сбербанка онлайн
    
        "ss": {
            "btnCardForPayment": "%XPATH%"   // xpath путь до карты, с которой будут проходить платежи
        }
    }
}
```
# Задать массив карт для совершения платежей: (файл routes/worker_sberbank.js)
```sh
var cards = [{ num: '%card_no%', amount: '%sum%'}, { num: '%card_no%', amount: '%sum%'}]; // где %card_no% - номер карты и %sum% - сумма перевода
```

# Запуск сервера:
```sh
cd /var/www/worker_money_send/ && npm start
go: http://localhost:3000/money/send/sberbank
```