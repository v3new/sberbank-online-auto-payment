var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('cookie-session');
var swig = require('swig');
var favicon = require('serve-favicon');
var logger = require('morgan');
var path = require('path');
var flash = require('connect-flash');

module.exports = function (app, config, crypto) 
{
	app.use(logger('dev'));
	app.enable('trust proxy');
	app.use(flash());
	
	app.engine('html', swig.renderFile);
	app.set('view engine', 'html');
	app.set('views', path.join(__dirname, '../', 'views'));
	
	app.set('view cache', false);
	swig.setDefaults({ cache: false });
	
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(cookieParser());
	app.use(session(config.get("session")));
};