var S = require('string');
var webdriverio = require('webdriverio');
var notifier = require('mail-notifier');

var options = {desiredCapabilities: { browserName: 'chrome' }};
var client = webdriverio.remote(options);

var input = '';

var cards = [];

module.exports = function (app, config, async) 
{
    var imap = {
        username: config.get("imap:login"),
        password: config.get("imap:password"),
        host: config.get("imap:host"),
        port: 993,
        tls: true,
        tlsOptions: { rejectUnauthorized: false }
    };
    
    notifier(imap).on('mail', function (mail) { 
        if (~mail.headers.subject.indexOf("SMS from 900")) {
            input = S(mail.text).between(' - ', '.').s;
        }
    }).start();
    
    var makePayment = async.queue(function (task, callback) {
        client
            .url(config.get("auth:sb:urls:payments"))
            .pause(20000)
            .setValue(config.get("auth:sb:ss:inpExternalCard"), task.num)
            .pause(5000)
            .click(config.get("auth:sb:ss:btnSelectCard"))
            .pause(10000)
            .click(config.get("auth:sb:ss:btnCardForPayment"))
            .pause(5000)
            .setValue(config.get("auth:sb:ss:inpAmount"), task.amount)
            .pause(5000)
            .click(config.get("auth:sb:ss:btnSendExternalCardNumber"))
            .pause(15000)
            .getText(config.get("auth:sb:ss:textExternalCard"), function (err, text) {
                console.log("----> send to: " + text);
                client
                    .click(config.get("auth:sb:ss:btnConfirmExternalCardNumber"))
                    .pause(15000)
                    .getText(config.get("auth:sb:ss:textSelectCard"), function (err, text) {
                        console.log("----> send from: " + text);
                        client
                            .click(config.get("auth:sb:ss:btnGetSMSToConfirmExternal"))
                            .call(function () {
                                console.log("----> wait for sms...")
                            })
                            .pause(65000)
                            .call(function () {
                                console.log("----> sms: " + input.toString());
                                client
                                    .setValue(config.get("auth:sb:ss:inpConfirmSMSExternal"), input.toString())
                                    .pause(15000)
                                    .click(config.get("auth:sb:ss:btnMakePayments"))
                                    .pause(15000)
                                    .url(function (err, res) {
                                        console.log("----> done: " + config.get("auth:sb:urls:check_print") + res.value.slice(res.value.lastIndexOf("=")+1))
                                        //client.end();
                                        callback();
                                    });
                            })
                    })
            })
    });
    
    app.get('/money/send/sberbank', function(req, res, next) {
        client
            .init()
            .url(config.get("auth:sb:urls:index"))
            .setValue(config.get("auth:sb:ss:inpLogin"), config.get("auth:sb:login"))
            .setValue(config.get("auth:sb:ss:inpPassword"), config.get("auth:sb:password"))
            .click(config.get("auth:sb:ss:btnLogin"))
            .call(function () {
                console.log("wait for sms...")
            })
            .pause(65000)
            .call(function () {
                console.log("sms: " + input.toString());
                client
                    .setValue(config.get("auth:sb:ss:inpConfirmSMS"), input.toString())
                    .click(config.get("auth:sb:ss:btnSendConfirmSMSLogin"))
                    .pause(15000)
                    .getText(config.get("auth:sb:ss:textUserName"), function (err, text) {
                        console.log("--> login AS: " + text);
                        async.forEachOf(cards, function (task, key, callback) {
                            makePayment.push(task, function (err) {
                                console.log('----> finished to: ' + task.num + ' ' + task.amount + 'rur.');
                                callback();
                            });
                        })
                    })
            });

        res.render('worker_sberbank', { title: 'SBERBANK.ONLINE' });
        
    });
};