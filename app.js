var config = require("nconf");
var express = require('express');

var mongoose = require('mongoose');
var request = require('request');
var crypto = require("crypto");
var async = require('async');

config.argv().env().file({ file: 'config.json' });

  var app = express();
  var models = {};

  	app.use(express.static('public'));

	require("./boot/mongoose")(config, mongoose, models);
	require("./boot/express")(app, config, crypto);

	require('./routes/index')(app, config, models, async);

module.exports = app;